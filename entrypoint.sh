#!/bin/bash

export ZEPHYR_BASE=/home/user/zephyr
cd /home/user

#Detect uninitialized directory and initialize if necessary
if [ ! -d "zephyr" ]; then
    west init
    west update
fi

$@