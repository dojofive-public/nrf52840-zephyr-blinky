# Demo of Zephyr running Blinky on NRF52840DK

Enjoy the wonders of a blinky app running on the NRF52840DK under Zephyr!

Includes support for debugging via VSCode.

Requires Docker to be installed. This greatly eases the burden of installing all the dependencies, especially on MacOS.


## Prerequisites:
- [Docker](https://docs.docker.com/get-docker/)
- [nRF Command Line Tools](https://www.nordicsemi.com/Software-and-tools/Development-Tools/nRF-Command-Line-Tools/Download)

## Build
- Run build.sh. The first time, it will take a long time as it seeds Zephyr. Second time is much faster.

## Flash
- Run flash.sh. Enjoy!