#!/bin/bash

PROJECT_PATH=zephyr/samples/basic/blinky

docker run --rm -v $(pwd):/home/user zephyrprojectrtos/zephyr-build west build -b nrf52840dk_nrf52840 "$PROJECT_PATH"